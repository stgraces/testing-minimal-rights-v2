Currently deployed an environment on the "devops" cluster in namespace "miles-microservice-platform". 

Flux v2 is deployed there and only watches its own namespace, I used the following config to deploy it:

cluster config from url: https://bitbucket.org/stgraces/devops-config/src/master/env/ in branch: test/devops
common-cluster-config from url: https://bitbucket.org/sofico/common-cluster-config/src/master/ from branch release/kbc/flux-v2 
helm values and commands in repo: https://bitbucket.org/stgraces/testing-minimal-rights-v2/src/master/ 
helm charts from repo: https://bitbucket.org/sofico/k8s-charts/src/master/ in branch: feature/DEVOPS-458
Most helmreleases are failing, because most helm chart create roles and minimal rights setup does not have access to roles.

If somebody could point me to a config (in clt for example) that works with minimal rights that I can use that would be helpful

Add secrets to `values.yaml` before using

`helm install flux-v2 ~/k8s-charts/charts/next/flux-v2 -f values.yaml -n miles-microservice-platform`

`helm install minimal-rights ~/k8s-charts/charts/next/cluster/miles-microservice-platform-global -f values-rbac.yml`